%include "words.inc"
%include "lib.inc"

%define BUFF_SIZE 256
%define NODE_SIZE 8

extern find_word

global _start

section .data

string_buffer:
	times BUFF_SIZE db 0

section .rodata

key_not_found:
	db "404", 0
buffer_overflow:
	db "АШИПКА ПЕРЕПОЛНЕНИЯ!!1!", 0

section .text

_start:
	xor rax, rax
	mov rdi, string_buffer
	mov rsi, BUFF_SIZE
	call read_word
	cmp rax, 0
	jne .success
	mov rdi, buffer_overflow
	call print_error
	call print_newline
	call exit
.success:
	mov rdi, rax
	mov rsi, w0
	push rdx
	call find_word
	pop rdx
	cmp rax, 0
	jne .key_ok
	jmp .key_err	
.key_ok:				
	add rax, NODE_SIZE
	add rax, rdx
	add rax, 1
	mov rdi, rax
	call print_string
	call print_newline
	call exit
.key_err:				
	mov rdi, key_not_found
	call print_error
	call print_newline
	call exit
